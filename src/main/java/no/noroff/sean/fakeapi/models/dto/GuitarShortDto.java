package no.noroff.sean.fakeapi.models.dto;

public class GuitarShortDto {
    public String getGuitarModel() {
        return guitarModel;
    }

    public void setGuitarModel(String guitarModel) {
        this.guitarModel = guitarModel;
    }

    private String guitarModel;

    public GuitarShortDto(String guitarModel) {
        this.guitarModel = guitarModel;
    }
}
