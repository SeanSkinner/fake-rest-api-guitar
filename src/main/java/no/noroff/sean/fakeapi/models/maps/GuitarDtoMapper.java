package no.noroff.sean.fakeapi.models.maps;

import no.noroff.sean.fakeapi.models.domain.Guitar;
import no.noroff.sean.fakeapi.models.dto.GuitarShortDto;
import org.springframework.stereotype.Component;

@Component
public class GuitarDtoMapper {

    public GuitarShortDto mapGuitarShortDto(Guitar guitar) {
        var shortGuitar = new GuitarShortDto(guitar.getModel().toUpperCase());
        return shortGuitar;
    }
}
