package no.noroff.sean.fakeapi.models.dto;

public class GuitarBrandTotalDto {
    private String brand;
    private int total;

    public GuitarBrandTotalDto(String brand, int total) {
        this.brand = brand;
        this.total = total;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
