package no.noroff.sean.fakeapi.dataaccess;

import no.noroff.sean.fakeapi.models.domain.Guitar;
import no.noroff.sean.fakeapi.models.dto.GuitarShortDto;

import java.util.List;


public interface IGuitarRepository {
    List<Guitar> getAllGuitars();
    Guitar getGuitar(int id);
    Guitar addGuitar(Guitar guitar);
    Guitar replaceGuitar(Guitar guitar);
    Guitar modifyGuitar(Guitar guitar);
    void deleteGuitar(int id);
    boolean guitarExists(int id);
    GuitarShortDto getGuitarShort(int id);
}
