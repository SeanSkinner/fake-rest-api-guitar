package no.noroff.sean.fakeapi.dataaccess;

import no.noroff.sean.fakeapi.models.domain.Guitar;
import no.noroff.sean.fakeapi.models.dto.GuitarBrandTotalDto;
import no.noroff.sean.fakeapi.models.dto.GuitarShortDto;
import no.noroff.sean.fakeapi.models.maps.GuitarDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class GuitarRepository implements IGuitarRepository {
    private Map<Integer, Guitar> guitars = seedGuitars();
    private GuitarDtoMapper guitarDtoMapper;

    @Autowired
    public GuitarRepository(GuitarDtoMapper guitarDtoMapper) {
        this.guitarDtoMapper = guitarDtoMapper;
    }

    private Map<Integer, Guitar> seedGuitars() {
        var guitars = new HashMap<Integer, Guitar>();

        guitars.put(1, new Guitar(1, "Fender", "Telecaster"));
        guitars.put(2, new Guitar(2, "Jackson", "Kelly"));
        guitars.put(3, new Guitar(3, "Yamaha", "FG500"));

        return guitars;
    }

    @Override
    public List<Guitar> getAllGuitars() {
        return guitars.values().stream().toList();
    }

    @Override
    public Guitar getGuitar(int id) {
        return guitars.get(id);
    }

    @Override
    public Guitar addGuitar(Guitar guitar) {
        guitars.put(guitar.getId(), guitar);
        return getGuitar(guitar.getId());
    }

    @Override
    public Guitar replaceGuitar(Guitar guitar) {
        var guitarToDelete = getGuitar(guitar.getId());
        guitars.remove(guitarToDelete);

        guitars.put(guitar.getId(), guitar);
        return getGuitar(guitar.getId());
    }

    @Override
    public Guitar modifyGuitar(Guitar guitar) {
        var guitarToModify = getGuitar(guitar.getId());

        guitarToModify.setModel(guitar.getModel());
        guitarToModify.setBrand(guitar.getBrand());

        return guitarToModify;
    }

    @Override
    public void deleteGuitar(int id) {
        guitars.remove(id);
    }

    @Override
    public boolean guitarExists(int id) {
        return guitars.containsKey(id);
    }

    @Override
    public GuitarShortDto getGuitarShort(int id) {
        var guitar = getGuitar(id);
        return guitarDtoMapper.mapGuitarShortDto(guitar);
    }

    public boolean isValidGuitar(Guitar guitar) {
        return guitar.getId() > 0 && guitar.getBrand() != null && guitar.getModel() != null;
    }

    public boolean isValidGuitar(Guitar guitar, int id) {
        return isValidGuitar(guitar) && guitar.getId() == id;
    }

    public List<GuitarBrandTotalDto> getBrandTotals() {
        return guitars
                .values()
                .stream()
                .collect(Collectors.groupingBy(Guitar::getBrand, Collectors.counting()))
                .entrySet()
                .stream()
                .map(g -> new GuitarBrandTotalDto(g.getKey(), g.getValue().intValue()))
                .collect(Collectors.toList());
    }
}
