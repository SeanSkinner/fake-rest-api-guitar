package no.noroff.sean.fakeapi.controllers;

import no.noroff.sean.fakeapi.dataaccess.GuitarRepository;
import no.noroff.sean.fakeapi.models.domain.Guitar;
import no.noroff.sean.fakeapi.models.dto.GuitarBrandTotalDto;
import no.noroff.sean.fakeapi.models.dto.GuitarShortDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/guitars")
public class GuitarController {
    private GuitarRepository guitarRepository;

    @Autowired
    public GuitarController(GuitarRepository guitarRepository) {
        this.guitarRepository = guitarRepository;
    }

    @GetMapping
    public ResponseEntity<List<Guitar>> getAllGuitars() {
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    @GetMapping("/{id}/short")
    public ResponseEntity<GuitarShortDto> getShortGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(guitarRepository.getGuitarShort(id), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Guitar> getGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(guitarRepository.getGuitar(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Guitar> createGuitar(@RequestBody Guitar guitar) {
        if(!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(guitarRepository.addGuitar(guitar), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Guitar> replaceGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        if(!guitarRepository.isValidGuitar(guitar, id)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if (!guitarRepository.guitarExists(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        guitarRepository.replaceGuitar(guitar);
        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @PatchMapping("/{id}")
    public Guitar modifyGuitar(@PathVariable int id, @RequestBody Guitar guitar) {
        return guitarRepository.modifyGuitar(guitar);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteGuitar(@PathVariable int id) {
        if (!guitarRepository.guitarExists(id)) {
            return new ResponseEntity<>("Guitar with id " +  id + " does not exist", HttpStatus.NOT_FOUND);
        }

        guitarRepository.deleteGuitar(id);
        return new ResponseEntity<>("Guitar with id " + id + " has been deleted", HttpStatus.NO_CONTENT);
    }

    @GetMapping("/brands")
    public ResponseEntity<List<GuitarBrandTotalDto>> getBrandTotals() {
        return new ResponseEntity<>(guitarRepository.getBrandTotals(), HttpStatus.OK);
    }
}
